import React, { Component } from "react";
import NavBar from "./components/navbar";
import "./App.css";
import Counters from "./components/counters";

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 0 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 },
      { id: 5, value: 0 }
    ],
    totalitems: 0
  };

  hanleIncrement = counter => {
    const counters = [...this.state.counters]; //spread operater
    console.log("values of counters");
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };

  hanleDecrement = counter => {
    const counters = [...this.state.counters]; //spread operater
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    if (counters[index].value > 0) {
      counters[index].value--;
    }

    this.setState({ counters });
  };
  handleReset = () => {
    const counters = this.state.counters.map(c => {
      c.value = 0;
      console.log("hello");

      // return c;
    });
    this.setState({ counters });
  };

  handleDelete = product => {
    const counters = this.state.counters.filter(c => c.id !== product);
    console.log("counterid value", product);
    this.setState({ counters });
  };

  handleItem = () => {
    let totalitems = [...this.state.totalitems];
    totalitems++;
    console.log("total items", totalitems);
    this.setState({ totalitems });
  };

  render() {
    console.log("values :---- ", this.state.counters);
    return (
      <React.Fragment>
        <NavBar
          totalCounters={this.state.counters.filter(c => c.value > 0).length}
          totalitems={this.state.counters
            .map(data => data.value)
            .reduce((accumulator, currentValue) => accumulator + currentValue)}
        />
        <main className="container">
          <Counters
            counters={this.state.counters}
            onReset={this.handleItem}
            onIncrement={this.hanleIncrement}
            onDecrement={this.hanleDecrement}
            onDelete={this.handleDelete}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
