import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  render() {
    const {
      counters,
      onIncrement,
      onDelete,
      onReset,
      onDecrement
    } = this.props;

    return (
      <div className="bg-light">
        {counters.map(counter => (
          <Counter
            key={counter.id}
            onIncrement={() => onIncrement(counter)}
            onDecrement={() => onDecrement(counter)}
            onDelete={() => onDelete(counter.id)}
            counter={counter}
          />
        ))}
        <center>
          <button onClick={onReset} className="btn btn-primary btn-sm m-2">
            Reset
          </button>
          <button className="btn btn-primary btn-sm m-2">New Entry</button>
        </center>
      </div>
    );
  }
}

export default Counters;
