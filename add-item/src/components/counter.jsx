import React, { Component } from "react";
class Counter extends Component {
  render() {
    return (
      <div>
        <center>
          <span className={this.getBadgeClasses()}>{this.fomateCount()}</span>
          <button
            onClick={() =>
              this.props.onIncrement({ id: this.props.counter.id })
            }
            className="btn btn-secondary m-2 btn-sm"
          >
            +
          </button>
          <button
            onClick={() =>
              this.props.onDecrement({ id: this.props.counter.id })
            }
            className="btn btn-secondary btn-sm"
          >
            -
          </button>
          <button
            onClick={() => this.props.onDelete({ id: this.props.counter.id })}
            className="btn btn-danger btn-sm m-2"
          >
            Delete
          </button>
        </center>
      </div>
    );
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.props.counter.value === 0 ? "warning" : "info";
    return classes;
  }

  fomateCount() {
    const { value } = this.props.counter;
    return value <= 0 ? <h6>Zero</h6> : <h6> {value}</h6>;
  }
}

export default Counter;
