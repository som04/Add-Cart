import React, { Component } from "react";

//stateless functional component

const NavBar = ({ totalCounters, totalitems }) => {
  console.log("hehheheheh:- ", totalitems);
  return (
    <nav className="navbar navbar-light bg-info">
      <a className="navbar-brand" href="#">
        MY CART ({" "}
        <span className="badge badge-pill badge-secondary">
          {totalCounters}
        </span>{" "}
        )
      </a>
      <a className="navbar-brand" href="#">
        TOTAL ITEMS({" "}
        <span className="badge badge-pill badge-secondary">{totalitems}</span> )
      </a>
    </nav>
  );
};

export default NavBar;
