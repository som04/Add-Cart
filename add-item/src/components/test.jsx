// we are creating our first component here
// here we first import react and component class from the react module
import React, { Component } from "react";
class Counter extends Component {
  state = {
    count: 0,
    // imageUrl: "https://picsum.photos/600",
    tags: ["Akhand", "Vipin", "Dharmendra", "Preeti", "Shabaz", "Rohan"]
  };

  // constructor() {
  //   super();
  //   this.handleIncrement = this.handleIncrement.bind(this);
  // }

  handleIncrement = product => {
    console.log("product value ", product);
    this.setState({ count: this.state.count + 1 });
  };

  render() {
    return (
      <div>
        <span className={this.getBadgeClasses()}>{this.fomateCount()}</span>
        <button
          onClick={() => this.handleIncrement}
          className="btn btn-secondary btn-sm"
        >
          Increment
        </button>
      </div>
    );
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.state.count === 0 ? "warning" : "primary";
    return classes;
  }

  fomateCount() {
    const { count } = this.state;
    return count === 0 ? <h1>Zero</h1> : <h1> {count}</h1>;
  }
}

export default Counter;
